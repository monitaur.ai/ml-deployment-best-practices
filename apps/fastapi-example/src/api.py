import logging
from pathlib import Path

from fastapi import APIRouter, Depends, Request
from fastapi.templating import Jinja2Templates
from joblib import load

from src.config import Settings, get_settings
from src.model.prediction import get_prediction
from src.pydantic import DiabetesModel


log = logging.getLogger("uvicorn")
templates = Jinja2Templates(directory="src/templates")
router = APIRouter()

try:
    # load model object
    dir = Path(__file__).resolve(strict=True).parent
    model_file_path = Path(dir).joinpath("model", "diabetes.joblib")
    model = load(model_file_path)
except Exception as e:
        log.error(f"Error: {e}")
        
@router.get("/")
def home(request: Request):
    return templates.TemplateResponse("home.html", {"request": request})


@router.get("/ping")
async def pong(settings: Settings = Depends(get_settings)):
    return {
        "ping": "pong!",
        "environment": settings.environment
    }


@router.post("/predict")
def predict(data: DiabetesModel):
    try:
        # extract data in correct order
        data_dict = data.dict()
        log.info(data_dict)

        # predict if the individual has diabetes or not
        # you should run this in the background with a task or message queue like celery
        prediction = get_prediction(model, data_dict)        

        # log and return prediction
        log.info(prediction)
        return {"prediction": prediction}
    except Exception as e:
        log.error(f"Error: {e}")
        return {"error": f"{e}"}
