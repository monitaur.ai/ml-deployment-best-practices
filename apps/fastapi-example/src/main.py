import logging

from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles

from src import api

log = logging.getLogger("uvicorn")


def create_application() -> FastAPI:
    application = FastAPI()
    application.include_router(api.router)
    application.mount("/static", StaticFiles(directory="src/static"), name="static")

    return application


app = create_application()


@app.on_event("startup")
async def startup_event():
    log.info("Starting up...")
    # add start up events here, like initializing a database


@app.on_event("shutdown")
async def shutdown_event():
    log.info("Shutting down...")