const button = document.querySelector("button");

button.addEventListener("click", (event) => {
  document.getElementsByClassName("message")[0].style.display = "none";

  const payload = {}
  const inputs = document.getElementsByTagName("input");
  const labels = document.getElementsByTagName("label");

  for (let index = 0; index < inputs.length; index++) {
    if (inputs[index].value) {
      const label = labels[index].textContent
      payload[label] = inputs[index].value
    } else {
      updateText("All fields are required.");
      showResults();
      return false;
    }
  }

  getPrediction(payload);
});


function getPrediction(data) {
  fetch("/predict", {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(data)
  })
  .then((response) => response.json())
  .then((data) => {
    console.log(data);
    updateText(data["prediction"]);
    showResults();
  })
  .catch((err) => {
    updateText(err);
    showResults();
  })
}

function hideResults() {
  document.getElementsByClassName("message")[0].style.display = "none";
}

function showResults() {
  document.getElementsByClassName("message")[0].style.display = "block";
}

function updateText(text) {
  document.getElementById("results").textContent = text;
}
