## FastAPI Example

Build the images and spin up the containers:

```sh
$ docker-compose up -d --build
```

Navigate to [http://localhost:8005/ping](http://localhost:8005/ping). You should see:

```sh
{
  "ping": "pong!",
  "environment": "prod"
}
```

Test it out at [http://localhost:8005](http://localhost:8005):

![](../example.gif)

You can view the Swagger UI at [http://localhost:8005/docs](http://localhost:8005/docs).

![](../inference.gif)

## API Examples

Python via [requests](https://docs.python-requests.org/en/master/):

```python
import requests
import numpy as np

to_predict_dict = {
    "Pregnancies": int(np.random.randint(0, 6, size=1)[0]),
    "Glucose": int(np.random.randint(99, 141, size=1)[0]),
    "BloodPressure": int(np.random.randint(64, 98, size=1)[0]),
    "SkinThickness": int(np.random.randint(0, 32, size=1)[0]),
    "Insulin": int(np.random.randint(0, 130, size=1)[0]),
    "BMI": float(np.random.randint(27, 36, size=1)[0]),
    "DiabetesPedigreeFunction": float(round(np.random.uniform(.2, .6, size=1)[0], 2)),
    "Age": int(np.random.randint(24, 40, size=1)[0])
}

print(to_predict_dict)

url = "http://localhost:8005/predict"
response = requests.post(url, json=to_predict_dict)
print(response.json())
```

curl:

```bash
$ curl -XPOST -H "Content-type: application/json" -d '{
  "Pregnancies": 2,
  "Glucose": 100,
  "BloodPressure": 75,
  "SkinThickness": 20,
  "Insulin": 100,
  "BMI": 29.1,
  "DiabetesPedigreeFunction": 0.52,
  "Age": 39
}' 'http://localhost:8005/predict'
```