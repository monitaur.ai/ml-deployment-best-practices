from pathlib import Path

from flask import Blueprint, current_app, jsonify, render_template
from flask_pydantic import validate
from joblib import load

from src.model.prediction import get_prediction
from src.pydantic import DiabetesModel

api = Blueprint("api", __name__)


@api.get("/")
def home():
    return render_template("home.html")


@api.get("/ping")
def pong():
    return jsonify(ping="pong", environment=current_app.config["ENVIRONMENT"])


@api.post("/predict")
@validate()
def predict(body: DiabetesModel):
    try:
        # import model object
        dir = Path(__file__).resolve(strict=True).parent
        model_file_path = Path(dir).joinpath("model", "diabetes.joblib")
        model = load(model_file_path)

        # extract data in correct order
        data_dict = body.dict()
        current_app.logger.info(data_dict)
        
        # predict if the individual has diabetes or not
        # you should run this in the background with a task or message queue like celery
        prediction = get_prediction(model, data_dict)

        # log and return prediction
        current_app.logger.info(prediction)
        return {"prediction": prediction}
    except Exception as e:
        current_app.logger.error(f"Error: {e}")
        return {"error": f"{e}"}
