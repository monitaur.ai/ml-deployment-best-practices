import numpy as np


def preprocessing(data_dict):
    """
    Description:
    Function to hold all data preprocessing prior to predict function

    Parameters:
    data_dict: Python dictionary in the format:
                {'featureName': featureValue}

    Returns:
    Processed numpy array
    """
    # Create a numpy array off of the inputs
    processed_array = np.array([list(data_dict.values())])
    return processed_array


def predict(model, data_array):
    """
    Description:
    Function to make a prediction off of processed data and pre-trained model

    Parameters:
    model: loaded pre-trained model
    data_array: numpy array of processed data

    Returns:
    Raw prediction from model as an int
    """
    raw_prediction = model.predict(data_array)
    return raw_prediction


def postprocessing(raw_prediction):
    """
    Description:
    Function to take raw_prediction and apply logic for decision.

    Parameters:
    raw_prediction: int, array of raw model decision

    Returns:
    Refined prediction as a string
    """
    if raw_prediction[0] == 1:
        prediction = "You have diabetes"
    else:
        prediction = "You do not have diabetes"
    return prediction


def get_prediction(model, data_dict):
    """
    Description:
    Function to wrap preprocessing, predict and postprocessing functions

    Parameters:
    model: loaded pre-trained model
    data_dict: Python dictionary in the format:
              {'featureName': featureValue}

    Returns:
    Refined prediction as a string or int
    """

    processed_array = preprocessing(data_dict)
    raw_prediction = predict(model, processed_array)
    prediction = postprocessing(raw_prediction)

    return prediction
