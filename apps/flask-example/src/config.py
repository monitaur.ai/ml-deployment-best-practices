import os


class BaseConfig:
    ENVIRONMENT = os.getenv("ENVIRONMENT", "dev")


class DevelopmentConfig(BaseConfig):
    pass


class TestingConfig(BaseConfig):
    pass


class ProductionConfig(BaseConfig):
    pass
