# Deploying a Machine Learning Model

We created two example applications to illustrate how a Machine Learning model can be deployed as a microservice utilizing engineering best practices.

These examples also serve as a base for Monitaur clients wishing to use our Counterfactuals service. 

1. [FastAPI Example](apps/fastapi-example)
1. [Flask Example](apps/flask-example)

The model uses a *[prediction.py](apps/fastapi-example/src/model/prediction.py)* file, composed of four main functions:

1. `preprocessing`
1. `predict`
1. `postprocessing`
1. `get_prediction`

The `get_prediction` function is a wrapper that can be used to return a prediction with all of the pre and post-processing completed. Additional functions can be used each of them as needed. 

```python
import numpy as np


def preprocessing(data_dict):
    """
    Description:
    Function to hold all data preprocessing prior to predict function

    Parameters:
    data_dict: Python dictionary in the format:
                {'featureName': featureValue}

    Returns:
    Processed numpy array
    """
    # Create a numpy array off of the inputs
    processed_array = np.array([list(data_dict.values())])
    return processed_array


def predict(model, data_array):
    """
    Description:
    Function to make a prediction off of processed data and pre-trained model

    Parameters:
    model: loaded pre-trained model
    data_array: numpy array of processed data

    Returns:
    Raw prediction from model as an int
    """
    raw_prediction = model.predict(data_array)
    return raw_prediction


def postprocessing(raw_prediction):
    """
    Description:
    Function to take raw_prediction and apply logic for decision.

    Parameters:
    raw_prediction: int, array of raw model decision

    Returns:
    Refined prediction as a string
    """
    if raw_prediction[0] == 1:
        prediction = "You have diabetes"
    else:
        prediction = "You do not have diabetes"
    return prediction


def get_prediction(model, data_dict):
    """
    Description:
    Function to wrap preprocessing, predict and postprocessing functions

    Parameters:
    model: loaded pre-trained model
    data_dict: Python dictionary in the format:
              {'featureName': featureValue}

    Returns:
    Refined prediction as a string or int
    """

    processed_array = preprocessing(data_dict)
    raw_prediction = predict(model, processed_array)
    prediction = postprocessing(raw_prediction)

    return prediction
```
# What is Monitaur?

Monitaur is the AI governance software company. Our Monitaur Machine Learning Assurance Platform helps enterprises and their partners build and deploy responsible AI and machine learning models that business leaders, regulators and consumers can trust. The platform’s integrated products – GovernML, RecordML, MonitorML, and AuditML – empower AI/ML teams to deliver transparency, fairness, safety and compliance for their high-impact systems. For more information, please visit https://www.monitaur.ai, and follow us on LinkedIn at https://www.linkedin.com/company/monitaur.

## Machine Learning Guidelines
Below are a set of general principles for the best practice of building and deploying machine learning and statistical models.
* Preprocessing, modeling, and postprocessing should ideally be encapsulated in one (or as few as possible) pipeline objects that can be saved. 
    * [Example Scikit Learn Pipeline](https://scikit-learn.org/stable/modules/compose.html#pipeline) that has been saved as a pickle. 
    * Pre/post-processing logic that is not in the pipeline object should reside in the prediction.py
* Model serving should be separated into a server file that loads the pipeline object (or model and processing pickle files) and a python file that has the preprocessing, model prediction, and post-processing logic.
* Encapsulate all models (including rules-based) and processing files into `.pickle`, `.joblib`, or `.onnx` for version control and reperformance.
* Adhere to a standard REST API deployment paradigm, as outlined in this repository.
* Model predictions should be online for decision transparency, robustness, and reperformance.
* Simpler is always better! Don't use Tensorflow if Scikit-Learn will suffice. Don't use a Random Forest if Linear Regression performs just as well.
* Document, document, document. 
    * Document your data lineage, provide a data dictionary, understand exactly where your data came from, and what it does. 
    * Document how your model performs and why specific decisions were made on feature selection, engineering, and model training.
* Random seeds were used when your model is trained to ensure consistent results.
* Your model has been evaluated independently for accuracy, protected class bias, and satisfying your initial intent.
